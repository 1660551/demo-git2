//ES5
var firstName1 = 'CyberSoft';
firstName1 = 'CyberSoft1';

var firstName1 = 'CyberSoft2';

// console.log(firstName1);

//ES6
let firstName2 = 'CyberSoft New';
firstName2 = 'CyberSoft New 2';
// let firstName2 = 'CyberSoft New3';
// console.log(firstName2);

const PI = 3.14;

//Không được phép gán giá trị
// PI = 3.1414
//Không được khai báo trùng tên
// const PI = 3.15;

// console.log(PI);

//Hoisting
//ES5 var

// console.log(sum(2,3));
// console.log(city);

// function sum(x,y){
//     return x+y;
// }

// console.log(NAME);
// console.log(address);
// console.log(city);

// const NAME = "CyberSoft";
// let address = "456 su Van Hanh";
// var city = "HCM";


// function weight(){
//     var w=100;
//     if(w>160){
//         var nickName ="Bé Na";
//         console.log(nickName + " nặng " + w+" kg");
//     }
//     console.log(nickName + " hơi gầy");
// }
// weight();

// function declearation
// function tenham(thamso){

// }

// function expression
// let hello = function(name){
//     return "Hello " + name;
// }

// ES6
// let hello1 = (name) =>{
//     return "Hello1 " + name;
// }

// chỉ có 1 tham số
// let hello1 = name =>{
//     return "Hello1 " + name;
// }

// Nếu chỉ có 1 lệnh return
// let hello1 = name => "Hello1 " + name;

// console.log(hello("Bé Heo"));
// console.log(hello1("Bé Na"))


// let hello = (name) =>{
//     return "Hello1 " + name;
// }

// let hello = (name) 
// =>{
//     return "Hello1 " + name;
// }
// console.log(hello("Bé Heo"));

// let hocVien = {
//     hoTen: "Nguyễn Thị Học Viên",
//     lop:"Ngữ Văn 12",
//     diemThi:[10,9,8],
//     LayThongTinHocVien:function(){
//         this.diemThi.map((diem,index)=>{
//             console.log("Họ Tên: "+ this.hoTen + "-Lớp: "+ this.lop);
//             console.log("Điểm thi: "+ index+":" +diem);
//         });
//     }
// }

// hocVien.LayThongTinHocVien();

// let mangC= [1,2,3,4,'abc'];
// let MangD= mangC;
// let MangD= [5,6,...mangC];
// console.log(MangD)
// console.log("1"==49)

// function Father(name){
//     this.name = name;
//     this.createColorEyse = function(){
//         console.log("Black");
//     }
// }
// function Childrend(){
//     Father.apply(this,arguments);
//     this.createColorEyse = function(){
//         console.log("Blue");
//     }
// }
// var child = new Childrend("Thanh");
// child.createColorEyse();


// class Father{
//     constructor(name){
//         this.name = name;
//     }
//     createColorEyse(){
//         console.log("Black");
//     }
// }

// // extends
// class Childrend extends Father{
//     createColorSkin(){
//         console.log("Brown");
//     }
// }

// let child = new Childrend("Con");
// console.log(child.name);
// child.createColorEyse();

//Node JS
// var callSum = require('./sum')
// console.log(callSum(3,5));

//ES6
// import callSum from './sum.js';
// console.log("Cộng 2 số: "+ callSum(3,5));

// import {sum as callSum} from './sum.js'
// console.log("Cộng 2 số: "+ callSum(3,5));

// import {sum,sum2} from './sum.js'
// console.log("Cộng 2 số: "+ sum(3,5));
// console.log("Cộng 3 số: "+ sum2(3,5,8));

import * as sumFunction from './sum.js';
console.log("Cộng 2 số: "+ sumFunction.sum(3,5));
console.log("Cộng 3 số: "+ sumFunction.sum2(3,5,8));
let arr1 = [1, 2, 3]; 
let arr2 = arr1; 
arr1.push(4);
 arr2.push(5);
 console.log(arr1);
 console.log(arr2);
//  var arr1 = [1, 2, 3]; 
//  var arr2 = arr1; 
//  arr1.push(6);
//   arr2.push(7);
//   console.log(arr1);
//   console.log(arr2);

  var a = "df";
  var c = a;
  c = "hello1"
  console.log(a);
  console.log(c);


    

  let cong = (a,b)=>{
      console.log("hello")
  }

  cong(5,8);

  const calcSum = (
    ...numbers
     ) => { 
     let total = 0; 
     for (let i = 0; i < numbers.length; i++) { 
     total += numbers[i]; 
     } 
     console.log(total); 
    }; 
     
    calcSum(1, 2, 3, 4, 1, 123);