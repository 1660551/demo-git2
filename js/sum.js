function sum(a,b){
    return a + b;
}

function sum2(a,b,c){
    return a + b + c;
}

//Nodejs
// module.exports = sum;

//ES6
// export default sum;

export {sum,sum2};